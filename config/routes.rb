Rails.application.routes.draw do
  root :to => 'home#index'
  get '/products', :to => 'products#index'
  get '/demo' => 'home#demo'
  # get '/show/:id' => 'home#show'
  # get '/edit/:id' => 'home#edit'
  resources :home
  mount ShopifyApp::Engine, at: '/'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
