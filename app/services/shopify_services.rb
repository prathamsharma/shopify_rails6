class ShopifyServices
    attr_accessor :is_save, :message

    BASE_URL = "https://shopiteststore1.myshopify.com"
    HEADERS = {"Content-Type" => "application/json", "X-Shopify-Access-Token" => Shop.first.shopify_token}

    def initialize is_save, message
        @is_save = is_save
        @message = message
    end

    def self.add_metafields product, parameters
        begin
            url = "#{BASE_URL}/admin/api/2021-01/products/#{product.id}/metafields.json"
            resp = Faraday.post(url, parameters, HEADERS)
            response = JSON.parse(resp.body)
            if resp.status == 201
                self.new(true, "Updated Successfully")
            else
                self.new(false, response["errors"])
            end
        rescue => e
            self.new(false, e)
        end
    end

    def self.get_metafields product
        begin
            url = "#{BASE_URL}/admin/api/2021-01/products/#{product.id}/metafields.json"
            resp = Faraday.get(url, {}, HEADERS)
            response = JSON.parse(resp.body)
            if resp.status == 200
                metafields = response["metafields"].select{|f| (f["namespace"] == "global" && f["key"] == "wholeSalePrice")}
                self.new(true, (metafields.count > 0) ? metafields[0]["value"] : "")
            else
                self.new(false, response["errors"])
            end
        rescue => e
            p "Hi there, we have an expection: #{e}"
            self.new(false, e)
        end
    end
end