# frozen_string_literal: true

class HomeController < AuthenticatedController
  # include ShopifyApp::EmbeddedApp
  # include ShopifyApp::RequireKnownShop

  def index
    @shop_origin = current_shopify_domain
  end

  def demo
  end

  def show
    @product = ShopifyAPI::Product.find(params[:id])
  end

  def edit
    @product = ShopifyAPI::Product.find(params[:id])
    response = ShopifyServices.get_metafields(@product)
    @response_value = response.message if response.is_save
    if(!response.is_save)
      flash[:error] = response.message
    end
  end

  def update
    @product = ShopifyAPI::Product.find(params[:id])
    parameters = get_params @product, params
    response = ShopifyServices.add_metafields(@product, parameters)
    if(response.is_save)
      flash[:notice] = response.message
    else
      flash[:error] = response.message
    end
    redirect_to root_path
  end

  def get_params product, params
    parameters = '{ "metafield": { "namespace": "global", "key": "wholeSalePrice", "value": '+params["wholesale_price"]+', "value_type": "integer" } }'
  end
end
